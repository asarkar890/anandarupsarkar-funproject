import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class ClassSchedulerMain {

	private Map<Integer, Integer> creditCN = new HashMap<Integer, Integer>();
	private Map<Integer, List<Course>> courseByCredit = new HashMap<Integer, List<Course>>();

//Instantiating individual courses setting their property values
	List<CourseTiming.DaysofWeek> comparchlist = Arrays.asList(
			CourseTiming.DaysofWeek.M, CourseTiming.DaysofWeek.W);
	private Course comparch = new Course("ECS 201A", "CompArch", 4,
			new CourseTiming(comparchlist, 10, 0, 11, 0));

	List<CourseTiming.DaysofWeek> swlist = Arrays.asList(
			CourseTiming.DaysofWeek.Th, CourseTiming.DaysofWeek.F);
	private Course sw = new Course("ECS 260", "SoftwareEng", 4,
			new CourseTiming(swlist, 9, 0, 10, 30));

	List<CourseTiming.DaysofWeek> algolist = Arrays.asList(
			CourseTiming.DaysofWeek.M, CourseTiming.DaysofWeek.W,
			CourseTiming.DaysofWeek.F);
	private Course algo = new Course("ECS 222A", "Algo", 4, new CourseTiming(
			algolist, 13, 10, 14, 0));

	List<CourseTiming.DaysofWeek> graphicslist = Arrays.asList(
			CourseTiming.DaysofWeek.M, CourseTiming.DaysofWeek.Th);
	private Course graphics = new Course("ECS 270", "Graphics", 4,
			new CourseTiming(graphicslist, 14, 10, 15, 0));

	List<CourseTiming.DaysofWeek> databaseslist = Arrays.asList(
			CourseTiming.DaysofWeek.Tu, CourseTiming.DaysofWeek.Th);
	private Course databases = new Course("ECS 265", "Databases", 4,
			new CourseTiming(databaseslist, 16, 10, 17, 00));
	
	List<CourseTiming.DaysofWeek> multimedialist = Arrays.asList(
			CourseTiming.DaysofWeek.Tu, CourseTiming.DaysofWeek.Th);
	private Course multimedia = new Course("ECS 298", "Multimedia", 4,
			new CourseTiming(multimedialist, 12, 10, 1, 30));

	private List<Course> allcourses = new ArrayList<Course>();
	
	 
	
	public List<Course> getAllCoursesList() {
		allcourses.add(comparch);
		allcourses.add(sw);
		allcourses.add(algo);
		allcourses.add(graphics);
		allcourses.add(databases);
		allcourses.add(multimedia);
		return allcourses; 
	}

	public static void main(String args[]) {

/*Instantiate ScheduleGenerator object with the underlying domain of all courses offered for the 
*term, minimum credit and maximum credit allowed for the term.*/ 
		
		ScheduleGenerator sgenerate = new ScheduleGenerator(new ClassSchedulerMain().getAllCoursesList(), 
				8, 20);
	
/*Generate and print all possible optimal schedules which can be formed from the underlying 
  domain of all courses.
*/
		
		sgenerate.generateAllOptimalSchedules(); 
		sgenerate.printSchedules(); 
	}
}
