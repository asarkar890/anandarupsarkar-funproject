import java.util.Date;

public class Course {

/* A course has a unique id, a name, credit, and timing reflecting which 
days and times in the week it is offered.	
*/
	private String courseId;
	private String courseName;
	private int credit;
	private CourseTiming ct;

//Different constructors for initializing a course instance
	public Course(Course cr) {
		this.courseId = cr.courseId;
		this.courseName = cr.courseName; 
		this.credit = cr.credit;
		this.ct = cr.ct;		
	}
	
	
	public Course(String courseid, String coursename, int credit,
			CourseTiming ct) {
		this.courseId = courseid;
		this.courseName = coursename;
		this.credit = credit;
		this.ct = ct;
	}

//Getter methods for retrieving course attributes
	public CourseTiming getCourseTiming() {
		return ct;
	}
	
	public String getCourseName() {
		return courseName; 
	}
	
	public int getCredit() {
		return credit; 
	}
	
/*This method checks if a course overlaps in timing with another course. 	
If the start time hour of course A falls in between the start time and end time hour of 
course B or vice versa, then they are overlapping. The same logic applies for 
the minutes. 
*/
	public boolean overlapsWith(Course cr) {
		for (CourseTiming.DaysofWeek firstcrdw : this.ct.dweek) {
			for (CourseTiming.DaysofWeek secondcrdw : cr.ct.dweek) {
				if (firstcrdw.equals(secondcrdw)) {
					if (this.ct.getStarthours() >= cr.ct.getStarthours()
							&& this.ct.getStarthours() <= cr.ct.getEndhours()) {
						if (this.ct.getStartminutes() >= cr.ct.getStartminutes()) {
							if (cr.ct.getEndminutes() == 0)
								cr.ct.setEndminutes(60);
							if (this.ct.getStartminutes() <= cr.ct.getEndminutes())
								return true;
						}
					}
					if (this.ct.getEndhours() >= cr.ct.getStarthours()
							&& this.ct.getEndhours() <= cr.ct.getEndhours()) {
						if (this.ct.getEndminutes() >= cr.ct.getStartminutes()) {
							if (cr.ct.getEndminutes() == 0)
								cr.ct.setEndminutes(60);
							if (this.ct.getEndminutes() <= cr.ct.getEndminutes()) 
								return true;
						}
					}
					
					if (cr.ct.getStarthours() >= this.ct.getStarthours()
							&& cr.ct.getStarthours() <= this.ct.getEndhours()) {
						if (cr.ct.getStartminutes() >= this.ct.getStartminutes()) {
							if (this.ct.getEndminutes() == 0)
								this.ct.setEndminutes(60);
							if (cr.ct.getStartminutes() <= this.ct.getEndminutes())
									return true;
							
						}
					}
					if (cr.ct.getEndhours() >= this.ct.getStarthours()
							&& cr.ct.getEndhours() <= this.ct.getEndhours()) {
						if (cr.ct.getEndminutes() >= this.ct.getStartminutes()) {
							if (this.ct.getEndminutes() == 0)
								this.ct.setEndminutes(60);
							if (cr.ct.getEndminutes() <= this.ct.getEndminutes()) 
								return true;
						}
					}
				
				}
			}
		}
		return false;
	}
	
//This method checks if a course takes place during the lunch hours after 12 noon and before 1 pm
	public boolean takesPlaceDuringLunch() {
		if ((this.getCourseTiming().getStarthours() == 12 
			&& this.getCourseTiming().getStartminutes() >= 0 && this.getCourseTiming().getStartminutes() <=59)
			|| (this.getCourseTiming().getEndhours() == 12 && 
				this.getCourseTiming().getEndminutes() >=0 && this.getCourseTiming().getEndminutes() <= 59)) {
					return true; 
		} else 
			return false;
	}


}
