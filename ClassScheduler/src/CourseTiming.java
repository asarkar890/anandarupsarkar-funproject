import java.sql.Time;
import java.util.*;

public class CourseTiming {

//The following enum is a collection of constants, i.e., the days of the week.   
  public enum DaysofWeek{
	  M, Tu, W, Th, F 
  }
  private int starthours;
  private int endhours; 
  private int startminutes;
  private int endminutes;
  //DaysofWeek dw;
  List<DaysofWeek> dweek = new ArrayList<DaysofWeek>();
  
/*The following constructor initializes an instance of CourseTiming, setting the start    
 hour, start minutes, end hour and end minutes respectively.
 */  
  public  CourseTiming(List<DaysofWeek> dw,int starthours,int startminutes,int endhours,int endminutes){
	  this.dweek.addAll(dw);
	  this.starthours=starthours;
	  this.startminutes=startminutes;
      this.endhours=endhours;
      this.endminutes=endminutes;
      
  }

  public List<DaysofWeek> getDaysOfWeek() {
	
	return dweek;
  }

	public int getStarthours() {
		return starthours;
	}
	
	public void setStarthours(int starthours) {
		this.starthours = starthours;
	}
	
	public int getEndhours() {
		return endhours;
	}

	public void setEndhours(int endhours) {
		this.endhours = endhours;
	}

	public int getStartminutes() {
		return startminutes;
	}

	public void setStartminutes(int startminutes) {
		this.startminutes = startminutes;
	}
	
	public int getEndminutes() {
		return endminutes;
	}
	
	public void setEndminutes(int endminutes) {
		this.endminutes = endminutes;
	}
  

}
