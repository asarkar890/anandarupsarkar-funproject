# This is my README

I have developed a simple CourseScheduler application which generates a set of courses which can be taken by a student in a term. Given a set of all courses offered by the university in a particular quarter or semester, a student might like to find the optimum schedule containing a subset of all the courses offered, which best matches his/her criteria. The criteria can be that the course timings should not be overlapping, there should be no more than two courses each day, the gap between two consecutive courses each day should be at least two hours, there should be no courses during lunch etc. There can be multiple optimum schedules matching the student's criteria. Note that there can be multiple valid schedules containing a subset of all the courses offered, which meet the bare constraints satisfying the university policy (like what is the minimum and maximum credit that the sum of the credits of the courses in a schedule should add up to), but they may not be optimal. Optimal schedules are ones which satisfy both these bare constraints as well as the personalized constraints which a student is looking for.

How to Run this code:

1. From the command line terminal navigate to ClassScheduler/src
2. Run javac *.java (assuming you have a version of jdk installed and PATH is setup to point to the jdk installation directory/bin)
3. Run java ClassSchedulerMain

Skip steps 1 and 2 above and run step 3 from within ClassScheduler/bin,
if you just want to run the code, without 
making any changes to the source (like adding more courses etc.).

After this, as console output you should be able to see the set of all optimal schedules, which a student can take, given the underlying domain of all courses and minimum and maximum credit range for the term.

Note that as a good practice you should put your class files in the ClassScheduler/bin directory rather than putting them in src along with source files. 
Thus, while compiling the source files, run
javac *.java -d ../bin/ from ClassScheduler/src
which will generate the class files in ClassScheduler/bin/ 
directory, keeping it separate from the source files. Then run, 
java ClassSchedulerMain from ClassScheduler/bin.


Alternatively you can also set up this project in Eclipse, keeping your source (src) folder under your newly created project and building and running from within Eclipse.

Feel free to add more courses, delete courses or updates the existing courses from the underlying domain of all courses 
and/or change the minimum and maximum credit range allowed for the term (for all these, changes need to be made to 
ClassSchedulerMain.java at relevant places) and see how the results change. 




